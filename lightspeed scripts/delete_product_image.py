import requests
import json
import time

api_key = ''
api_secret = ''
product_count = 'https://{0}:{1}@api.webshopapp.com/en/products/count.json'.format(
    api_key, api_secret)
url_product = 'https://{0}:{1}@api.webshopapp.com/en/products.json?limit=250&page='.format(
    api_key, api_secret)

prod_count = requests.get(product_count)
prod_count_text = prod_count.text
product_count_json = json.loads(prod_count_text)
count_prod = product_count_json['count']
print(count_prod)

total_pages = round(count_prod / 250) + 1
print(total_pages)

if total_pages <= 1:
    start_page = 0
else:
    start_page = 1

for i in range(start_page, total_pages):
    target_url = url_product + str(i)
    print(target_url)
    product = requests.get(url_product)
    product_json = json.loads(product.text)
    for product in product_json['products']:
        if not product['image']:
            continue
        else:
            image_url = product['images']['resource']['link']
            image_url_auth = 'https://' + api_key + ':' + api_secret + '@' + image_url[8:]
            images = requests.get(image_url_auth)
            image_json = json.loads(images.text)
            for image in image_json['productImages']:
                id_image = image['id']
                time.sleep(1)
                requests.delete(
                    'https://{0}:{1}@api.webshopapp.com/en/products/{2}/images/{3}.json'.
                    format(api_key, api_secret, str(product['id']), str(id_image)))
                print('afbeelding van {0} is verwijderd.'.format(product['title']))
