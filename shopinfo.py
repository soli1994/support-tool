# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 15:18:10 2018

@author: Solaiman
"""
import os
import requests
import json
from flask import Flask, request, render_template

# from google_oauth_flask import set_oauth_redirect_endpoint
# from google_oauth_flask import login_required
app = Flask(__name__)

key = os.urandom(24)
SECRET_KEY = key


# app.config.update(
# SECRET_KEY             = key,
# GOOGLE_CONSUMER_KEY    = '',
# GOOGLE_CONSUMER_SECRET = '',
# OAUTH_URL              = '',
# OAUTH_TOKEN_URL        = '',
# OAUTH_USER_INFO_URL    = '',
# OAUTH_ALLOWED_DOMAINS  = ('',)
# )

# set_oauth_redirect_endpoint(app)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/klant', methods=['POST'])
def klant():
    username = request.form['username']
    password = request.form['password']
    shopnummer = request.form['shopnummer'].lstrip("0").replace(" ", "")
    v2_url = 'https://' + shopnummer + '.webshopapp.com/admin/auth/login'
    user_url = 'https://' + shopnummer + '.webshopapp.com/admin/shop_users' \
                                         '.json '
    domein_url = 'https://' + shopnummer + '.webshopapp.com/admin/domains.json'
    shop_url = 'https://' + shopnummer + '.webshopapp.com/admin/settings' \
                                         '/company.json '
    flow_url = 'https://' + shopnummer + '.webshopapp.com/admin/settings' \
                                         '/workflow.json '
    # thema_url ='https://'+shopnummer+'.webshopapp.com/admin/themes.json'
    pay_url = 'https://' + shopnummer + '.webshopapp.com/admin' \
                                        '/payment_providers.json '
    shipping_url = 'https://' + shopnummer + '.webshopapp.com/admin' \
                                             '/shipment_methods '
    payload = {
        'login[email]':username,
        'login[password]':password
    }
    while True:
        try:
            with requests.Session() as session:
                session.post(v2_url, payload)
                user = session.get(user_url)
                text_user = user.text
                json_user = json.loads(text_user)
                shopurl = session.get(shop_url)
                text_shop = shopurl.text
                json_shop = json.loads(text_shop)
                flow = session.get(flow_url)
                text_flow = flow.text
                json_flow = json.loads(text_flow)
                # thema = session.get(thema_url)
                # text_thema = thema.text
                # json_thema = json.loads(text_thema)
                pay = session.get(pay_url)
                text_pay = pay.text
                json_pay = json.loads(text_pay)
                domein = session.get(domein_url)
                text_domein = domein.text
                json_domein = json.loads(text_domein)
                domeinnaam = json_domein['sub_domains'][0]['domain'][
                    'domain_name']
                cname_domein = json_domein['sub_domains'][0]['shop'][
                    'maindomain']
                dns_ns_url = 'https://dns-api.org/ns/' + domeinnaam
                dns_ns = requests.get(dns_ns_url)
                dns_ns_text = dns_ns.text
                json_ns_dns = json.loads(dns_ns_text)
                dns_a_url = 'https://dns-api.org/a/' + domeinnaam
                dns_a = requests.get(dns_a_url)
                dns_a_text = dns_a.text
                json_a_dns = json.loads(dns_a_text)
                dns_mx_url = 'https://dns-api.org/mx/' + domeinnaam
                dns_mx = requests.get(dns_mx_url)
                dns_mx_text = dns_mx.text
                json_mx_dns = json.loads(dns_mx_text)
                dns_cname_url = 'https://dns-api.org/cname/' + cname_domein
                dns_cname = requests.get(dns_cname_url)
                dns_cname_text = dns_cname.text
                json_cname_dns = json.loads(dns_cname_text)
                dns_txt_url = 'https://dns-api.org/txt/' + domeinnaam
                dns_txt = requests.get(dns_txt_url)
                dns_txt_text = dns_txt.text
                json_txt_dns = json.loads(dns_txt_text)
                # status_domein = 'https://dig.whois.com.au/whois/'+domeinnaam
                # response_review = requests.get(status_domein)
                # soup = BeautifulSoup(response_review.content,'html.parser')
                # domain_status = soup.find('table')
                # first_td = domain_status.findAll('td')
                # status = first_td[3].text
                shopid = json_shop['shop']['id']
                v1_url = 'https://seoshop.webshopapp.com/backoffice/admin' \
                         '-shops/edit?id=' + str(
                    shopid)
                cname_check = json_shop['shop']['cname_url'] + '.'
                payment_url = 'https://' + shopnummer + '.webshopapp.com/admin/payment_providers'
            return render_template('klant_info.html', json_a_dns=json_a_dns,
                                   json_ns_dns=json_ns_dns,
                                   json_cname_dns=json_cname_dns,
                                   domeinnaam=domeinnaam, json_user=json_user,
                                   shopnummer=shopnummer, v1_url=v1_url,
                                   v2_url=v2_url,
                                   json_mx_dns=json_mx_dns, shopid=shopid,
                                   cname_check=cname_check,
                                   json_flow=json_flow,
                                   json_txt_dns=json_txt_dns,
                                   json_pay=json_pay, payment_url=payment_url,
                                   shipping_url=shipping_url
                                   , json_shop=json_shop)
        except IndexError:
            domeinnaam = 'example.com'
            cname_domein = 'web.whatsapp.com'
            dns_ns_url = 'https://dns-api.org/ns/' + domeinnaam
            shopurl = session.get(shop_url)
            text_shop = shopurl.text
            json_shop = json.loads(text_shop)
            dns_ns = requests.get(dns_ns_url)
            dns_ns_text = dns_ns.text
            json_ns_dns = json.loads(dns_ns_text)
            dns_a_url = 'https://dns-api.org/a/' + domeinnaam
            dns_a = requests.get(dns_a_url)
            dns_a_text = dns_a.text
            json_a_dns = json.loads(dns_a_text)
            dns_mx_url = 'https://dns-api.org/mx/' + domeinnaam
            dns_mx = requests.get(dns_mx_url)
            dns_mx_text = dns_mx.text
            json_mx_dns = json.loads(dns_mx_text)
            dns_cname_url = 'https://dns-api.org/cname/' + cname_domein
            dns_cname = requests.get(dns_cname_url)
            dns_cname_text = dns_cname.text
            json_cname_dns = json.loads(dns_cname_text)
            # status_domein = 'https://dig.whois.com.au/whois/'+domeinnaam
            # response_review = requests.get(status_domein)
            # soup = BeautifulSoup(response_review.content,'html.parser')
            # domain_status = soup.find('table')
            # first_td = domain_status.findAll('td')
            # status = first_td[3].text
            dns_txt_url = 'https://dns-api.org/txt/' + domeinnaam
            dns_txt = requests.get(dns_txt_url)
            dns_txt_text = dns_txt.text
            json_txt_dns = json.loads(dns_txt_text)
            shopid = json_shop['shop']['id']
            v1_url = 'https://seoshop.webshopapp.com/backoffice/admin-shops' \
                     '/edit?id=' + str(
                shopid)
            cname_check = json_shop['shop']['cname_url'] + '.'
            payment_url = 'https://' + shopnummer + '.webshopapp.com/admin' \
                                                    '/payment_providers '
            return render_template('klant_info.html', json_a_dns=json_a_dns,
                                   json_ns_dns=json_ns_dns,
                                   json_cname_dns=json_cname_dns,
                                   domeinnaam=domeinnaam, json_user=json_user,
                                   shopnummer=shopnummer, v1_url=v1_url,
                                   v2_url=v2_url,
                                   json_mx_dns=json_mx_dns, shopid=shopid,
                                   cname_check=cname_check,
                                   json_flow=json_flow,
                                   json_txt_dns=json_txt_dns,
                                   json_pay=json_pay, payment_url=payment_url,
                                   shipping_url=shipping_url
                                   , json_shop=json_shop)
        except KeyError:
            v1_url = 'https://seoshop.webshopapp.com/backoffice/admin-shops' \
                     '/edit?id=' + str(
                shopnummer)
            error = 'Een shop met dit nummer kan niet worden gevonden, ' \
                    'weet je wel zeker dat dit nummer correct is? '
            return render_template('index.html', error=error, v1_url=v1_url)
        except requests.exceptions.SSLError as e:
            geen_domein = 'Gebruik geen domein om te zoeken naar een shop, ' \
                          'probeer het nogmaals met een geldig shopnummer. '
            return render_template('index.html', geen_domein=geen_domein)
        except requests.exceptions.ConnectionError as r:
            geen_speciale_tekens = 'Gebruik geen speciale tekens, probeer ' \
                                   'het nogmaals met een shopnummer. '
            return render_template('index.html',
                                   geen_speciale_tekens=geen_speciale_tekens)
        except requests.exceptions.InvalidURL as ze:
            geen_speciale_tekens = 'Gebruik geen speciale tekens, probeer ' \
                                   'het nogmaals met een shopnummer. '
            return render_template('index.html',
                                   geen_speciale_tekens=geen_speciale_tekens)
        except UnicodeError:
            geen_speciale_tekens = 'Gebruik geen speciale tekens, probeer ' \
                                   'het nogmaals met een shopnummer. '
            return render_template('index.html',
                                   geen_speciale_tekens=geen_speciale_tekens)
        except json.decoder.JSONDecodeError as error:
            pay = session.get(pay_url)
            text_pay = pay.text
            json_pay = json.loads(text_pay)
            flow = session.get(flow_url)
            text_flow = flow.text
            json_flow = json.loads(text_flow)
            domeinnaam = 'example.com'
            cname_domein = 'web.whatsapp.com'
            dns_ns_url = 'https://dns-api.org/ns/' + domeinnaam
            shopurl = session.get(shop_url)
            text_shop = shopurl.text
            json_shop = json.loads(text_shop)
            dns_ns = requests.get(dns_ns_url)
            dns_ns_text = dns_ns.text
            json_ns_dns = json.loads(dns_ns_text)
            dns_a_url = 'https://dns-api.org/a/' + domeinnaam
            dns_a = requests.get(dns_a_url)
            dns_a_text = dns_a.text
            json_a_dns = json.loads(dns_a_text)
            dns_mx_url = 'https://dns-api.org/mx/' + domeinnaam
            dns_mx = requests.get(dns_mx_url)
            dns_mx_text = dns_mx.text
            json_mx_dns = json.loads(dns_mx_text)
            dns_cname_url = 'https://dns-api.org/cname/' + cname_domein
            dns_cname = requests.get(dns_cname_url)
            dns_cname_text = dns_cname.text
            json_cname_dns = json.loads(dns_cname_text)
            dns_txt_url = 'https://dns-api.org/txt/' + domeinnaam
            dns_txt = requests.get(dns_txt_url)
            dns_txt_text = dns_txt.text
            json_txt_dns = json.loads(dns_txt_text)
            shopid = json_shop['shop']['id']
            v1_url = 'https://seoshop.webshopapp.com/backoffice/admin-shops' \
                     '/edit?id=' + shopnummer
            cname_check = json_shop['shop']['cname_url'] + '.'
            payment_url = 'https://' + shopnummer + '.webshopapp.com/admin' \
                                                    '/payment_providers '
            return render_template('klant_info.html', json_a_dns=json_a_dns,
                                   json_ns_dns=json_ns_dns,
                                   json_cname_dns=json_cname_dns,
                                   domeinnaam=domeinnaam, json_user=json_user,
                                   shopnummer=shopnummer, v1_url=v1_url,
                                   v2_url=v2_url,
                                   json_mx_dns=json_mx_dns, shopid=shopid,
                                   cname_check=cname_check,
                                   json_flow=json_flow,
                                   json_txt_dns=json_txt_dns,
                                   json_pay=json_pay, payment_url=payment_url,
                                   shipping_url=shipping_url
                                   , json_shop=json_shop)


@app.errorhandler(403)
def FoutieveLogin(error):
    quote_get = requests.get(
        "https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous"
        "&count=1",
        headers={
            "X-Mashape-Key":"pJUFlAXzc1mshUsKBEVyVAvGnxeTp1efJInjsnemExnzOrlyAb",
            "Accept":"application/json"
        })
    quote_text = quote_get.text
    quote_json = json.loads(quote_text)
    return render_template('403.html', quote_json=quote_json)


if __name__ == '__main__':
    app.run(debug=True)
